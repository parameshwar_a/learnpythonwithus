# Let's write this core more efficiently with functions

def printer(content, design, num):
    print(design*num)
    print(content.center(num, design))
    print(design*num)

def reporter(datalist, c_score, need_score):
    print("Scored points: ", sum(datalist))
    print("No of games played: ", len(datalist))
    c_score+=sum(datalist)
    print("Needed points: ",need_score-c_score)
    return c_score

printer("weekly", "#", 20)

data = [ "10 3 8 15", "20 7 9", "12 4 4 4", "17 2 10 15 20"]
required_score=500
cumulative_score=0
for each_day_data in data:
    todays_data = list(map(int, each_day_data.split()))
    cumulative_score=reporter(todays_data, cumulative_score, required_score)


printer("monthly", "#", 20)

monthly="12 3 6 20 13 8 9 5 16 19 4 3 8 6 14"
required_target=500
monthly_data=list(map(int, monthly.split()))
reporter(monthly_data, 0, required_target)


