
# Functions syntax in python

# def <func_name>(<params>):
#   indented body
#   indented body
#   indented body
#   return value

# print(func_name("dosa"))

def repeater(dsign, num):
    num=num**2
    print(dsign*num)

#print(dsign)

print("hello")
repeater("#", 5)
print("welcome")
repeater("#", 7)
print("command")
repeater("$", 7)

print("Hello, you are welcome")
repeater("-", 20)
print("This is hotel continental")

# Tryout challenge
# Write a function to print if the given number is odd or even
# write a function to check if the given string is palindrome
# Palindrome is string equals in both forward and in reverse

# "eye" == reverse of "eye"
# "eye" == "eye"

# "madam" == reverse of "madam"
# "madam" == "madam"
