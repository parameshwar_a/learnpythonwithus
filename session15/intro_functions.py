
# Functions are for re-usability
# It is a block of code that can be re-used to avoid repeatability 
# Just like variables, but variables are for values functins are for set of code itself.


# Let me take you guys to your childhood
# Sweet old memories 
# Baby steps...


# Are you still thinking about every step ?
# No right we automatically does
# It is not only in walking (breathing, eating, driving, brushing, bathing etc,..)

# Because they are stored in our sub-consious mind we do it automatically  when we think about it.
# Just like that functions are like storing some steps in sub-consiuous of python so everytime you don't need to tell all the steps 
# Instead you just think about it.


# Let's see some practical python examples
# There is a requirement, that you need to print a report of how many points you scored everyday and how many games you played today and how much more score you needed to achieve target.

# You will get the each day games and scores in a space-seperated strings and all days data in a list.

print("#"*20)
print("Weekly".center(20, "#"))
print("#"*20)


data = [ "10 3 8 15", "20 7 9", "12 4 4 4", "17 2 10 15 20"]
required_score=500
cumulative_score=0
for each_day_data in data:
    todays_data = list(map(int, each_day_data.split()))
    print("Scored points: ", sum(todays_data))
    print("No of games played: ", len(todays_data))
    cumulative_score+=sum(todays_data)
    print("Need point: ", required_score-cumulative_score)

# For the same requirement you also need to create some monthly report.
# But you will get one by one month not in a list but in a string only.

print("#"*20)
print("Monthly".center(20, "#"))
print("#"*20)

monthly="12 3 6 20 13 8 9 5 16 19 4 3 8 6 14"
required_target=500
monthly_data=list(map(int, monthly.split()))
print("Scored points: ", sum(monthly_data))
print("No of games played: ", len(monthly_data))
print("Need point: ", required_score-sum(monthly_data))



#
