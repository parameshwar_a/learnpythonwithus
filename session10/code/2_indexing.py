fruits = {"apple", "papaya", "banana", "orange"}

# Indexing wont work

# For

for element in fruits:
    print(element)

# in not in

print("apple" in fruits)
print("orange" not in fruits)
