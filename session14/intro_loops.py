

#Loops are for repetition
#It is used to repeat the same thing over and over again without manual effort


#Imagine you want to print numbers from 1 to 100
#Do you write 100 print statements ?


#It is really hard right. Also what happens if you want to print 1000 elements
#It is more work.


#To avoid this kind of issues in repeating stuff.
#Programmers found loops. Which will repeat the content for the times you want to repeat.

#In python we have two types of loop

# 1 For loop

# 2 While loop

# Just a sample of loops

for num in range(1000):
    print("Hello world", num, end="")
    print()
