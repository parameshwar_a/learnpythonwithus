

#Loops are for repeating things multiple times
#Saves repetitive boring work

#We can write loops for that.
#syntax is very simple for <iterative key> in <iterable>:
#   Remember all contents of a loop needs to be indented.#   so please maintain the nice indents

#print(range(0,11))

#for i in range(0,100,2):
#    print(i)

#loops can also be useful if you are working with iterables like list, tuple, set and even dictionaries too

namelist=["paramesh","praveen", "ranjith", "Syed jafer"]
#a=10
#for name in namelist:
#    print(a)
#    print(name)
#

#Now you guys try it for tuple and sets

#Example let's say you have to assign roll number for a class.

#Adding number manually for every person is cumbersome work.





#for index in range(0, len(namelist)):
#    print(namelist[index], "2206"+str(index+1).rjust(3,"0"), end="")
#    print()

#for i in range(0,len(namelist)):
#    for j in range(i, i+1):
#        print(namelist[j], j, end="")
#        print()
#

for i in range(0, 2):
    for j in range(0,2):
        print(i,j)



#for num in range(0, 12):
#   print("2206"+str(num+1).rjust(3,"0"))
#

