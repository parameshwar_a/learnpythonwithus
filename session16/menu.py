
# Function definition
def sniper():
    print("Used for long range shot")
# Function definition
def pistol():
    print("Used for close range shot")

# Function definition
def machine_gun():
    print("Used for continuos firing")

# Just like normal key-value pair, instead of a value we can store the name of the function 
menu={
        1:sniper,
        2:pistol,
        3:machine_gun
        }
#once we get the value(function name) we put a () near to it so it can create a function call
menu[1]()

#after executing this will become
# sniper()
