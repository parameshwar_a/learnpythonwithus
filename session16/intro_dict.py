

# Python dictionaries are similar to real time dictionaries in structure,

#unlike other data structure, dictionary will be different.

# Dictionary will have words and respective meanings

# You look for the words based on that you will get defenitions.

#Similar in python dictionary syntax will be below
#
#datadict = {
#        key: value,
#        key2: value2,
#        key3: value3
#        }


# For accessing elements, you use .get() method or square brackets

# datadict.get(key) --> This statement returns value of that particular key

# datadict[key] -- > same as above

#imdb_rating = {
#        "Jai Bhim": 8.9,
#        "The Shawshank Redemption": 9.3,
#        "The Bourne Identity":7.9
#        }
#
#print(imdb_rating.get("Jai Bhim"))
#
#print(imdb_rating["The Bourne Identity"])
#
#Adding a key-value pair Same way

#imdb_rating["Harry Potter 1"] = 7.6
#
#print(imdb_rating)
#
#
#imdb_rating.update({"Ponniyin Selvan 1": 7.5})
#
#print(imdb_rating)
#
#
# Looping can also be done for keys/values/both

# We can use .keys() .values() .items() functions for looping

#for key in imdb_rating.keys():
#    print(key)
#
#
#for value in imdb_rating.values():
#    print(value)
#
#for key,value in imdb_rating.items():
#    print(key, value)
#
#

